# Awesome Inc Data Warehouse


## Goals

We would like to report on the number of installations that the company is doing every month in order to see if the business is growing.

We would also like to see which product category brings us more revenues, and which region of the world is our best market.

## Requirements

- [ ] Design a dimensional model capable of answering those questions, and possibly more;
- [ ] Implement this dimensional model using dbt;
- [ ] Version your code;
- [ ] Create the associated Power BI report (bonus)