with product_categories as (
  select * from {{ ref('stg_product_categories') }}
),
products as (
  select * from {{ ref('stg_products') }}
)

select 
    products.product_id,
    products.product_reference,
    products.product_name,
    product_categories.product_category_name

from products

left join product_categories on products.product_category_id = product_categories.product_category_id