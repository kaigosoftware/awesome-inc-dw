with installations as (
  select * from {{ ref('stg_installations') }}
),
products as (
  select * from {{ ref('stg_products') }}
),
customers as (
  select * from {{ ref('stg_customers') }}
)

select
    products.product_price as revenue,
    installations.product_id,
    customers.customer_id,
    customers.country_id,
    installations.installation_date

from installations

left join products on installations.product_id = products.product_id
left join customers on installations.customer_id = customers.customer_id
