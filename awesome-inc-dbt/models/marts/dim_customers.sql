with customers as (
  select * from {{ ref('stg_customers') }}
)

select 
  customers.customer_id,
  customers.customer_premium

from customers