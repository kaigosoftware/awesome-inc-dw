with date_dimension as (
    {{ dbt_date.get_date_dimension('2021-01-01', '2022-12-31') }}
)

select 
    date_dimension.*

from date_dimension
