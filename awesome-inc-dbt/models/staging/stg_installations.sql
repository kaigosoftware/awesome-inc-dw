with renamed as (
    select 
        installation.id as installation_id,
        installation.name as installation_name,
        installation.description as installation_description,
        installation.product_id,
        installation.customer_id,
        installation.installation_date

    from installation
)

select * from renamed