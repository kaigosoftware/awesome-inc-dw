with renamed as (
    select
        customer.id as customer_id,
        customer.name as customer_name,
        customer.email as customer_email,
        customer.country_id,
        customer.premium_customer as customer_premium

    from customer   
)

select * from renamed