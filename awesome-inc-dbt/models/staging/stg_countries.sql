with renamed as (
    select
        country.id as country_id,
        country.name as country_name,
        country.region as country_region

    from country   
)

select * from renamed
