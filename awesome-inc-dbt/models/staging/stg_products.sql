with renamed as (
    select
        product.id as product_id,
        product.reference as product_reference,
        product.name as product_name,
        product.category_id as product_category_id,
        {{try_cast('product.price', 'int')}} as product_price

    from product   
)

select * from renamed