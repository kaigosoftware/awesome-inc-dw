with countries as (
  select * from {{ ref('dim_countries') }}
),
installations as (
    select * from {{ ref ('fct_installations')}}
)

select
    sum(installations.revenue) as total_revenue,
    countries.country_region

from installations

left join countries on installations.country_id = countries.country_id

group by countries.country_region

order by total_revenue desc