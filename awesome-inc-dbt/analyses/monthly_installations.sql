with dates as (
  select * from {{ ref('dim_dates') }}
),
installations as (
    select * from {{ ref ('fct_installations')}}
)

select
    count(1),
    dates.month_of_year,
    dates.year_number

from installations

left join dates on dates.date_day = installations.installation_date

group by dates.month_of_year, dates.year_number

order by dates.month_of_year desc, dates.year_number desc
