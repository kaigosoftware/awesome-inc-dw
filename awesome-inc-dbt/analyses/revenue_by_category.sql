with products as (
  select * from {{ ref('dim_products') }}
),
installations as (
    select * from {{ ref ('fct_installations')}}
)

select
    sum(installations.revenue) as total_revenue,
    products.product_category_name

from installations

left join products on installations.product_id = products.product_id

group by products.product_category_name

order by total_revenue desc