with customers as (
  select * from {{ ref('dim_customers') }}
),
installations as (
    select * from {{ ref ('fct_installations')}}
)

select
    sum(installations.revenue) as total_revenue,
    customers.customer_premium

from installations

left join customers on installations.customer_id = customers.customer_id

group by customers.customer_premium

order by total_revenue desc